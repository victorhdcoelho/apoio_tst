data = {
        "cod_referencia": [],
        "titulo": [],
        "data": [],
        "nivel_des": [],
        "dim_sup": [],
        "nome_prod": [],
        "conteudo": [],
        "sis_ar": [],
        "cond_ace": [],
        "lan": [],
        "conservacao": [],
        "regras_conv": [],
        "data_cri": [],
        "lan2": [],
        "palavras_chaves": [],
}

aux = {
    "cod_referencia": {
                    "regex": r"\s*\w*((C|c)(o|ó)digo\s*de\s*refer(e|ê)ncia\s*.*\s*(\w+|-|\s*\S)*)\s*\w*",
                },
    "titulo": {
                    "regex": r"\s*\w*((T|t)(í|i)tulo\s*.*\s*(\s*\w*\s*)*.\s*(\w*.)*)",
                },
    "data": {
                    "regex": r"\s*\w*((D|d)ata\s*.*\s*((\w*).)*)\s*\w*",
                },
    "nivel_des": {
                    "regex": r"\s*\w*((N|n)(í|i)vel\s*de\s*Descriç(ã|a)o\s*.*\s*(\w*\s*)*)\s*\w*.*",
                },
    "dim_sup": {
                    "regex": r"\s*\w*((D|d)imens(a|ã)o\s*e\s*(S|s)uporte\s*.*\s*(\w*\s*.*)*)\s*\w*",
                },
    "nome_prod": {
                    "regex": r"\s*\w*((N|n)ome\s*dos\s*produtores\s*.*\s*(\w+\s*.)*)\s*",
                },
    "conteudo": {
                    "regex": r"\s*\w*((Â|A)mbito\s*e\s*conte(ú|u)do\s*.*\s*(\w*\s*.)*)\s*\w*",
                },
    "sis_ar": {
                    "regex": r"\s*\w*((S|s)istema\s*de\s*arranjo\s*.*\s*(\w*\s*.)*)\s*",
                },
    "cond_ace": {
                    "regex": r"\s*\w*((C|c)ondi(c|ç)(o|õ)es\s*de\s*acesso\s*.*\s*(\w*\s*.)*)\s*",
                },
    "lan": {
                    "regex": r"\s*\w*((I|i)dioma\s*.*\s*(\w*\s*.)*)\s*",
                },
    "conservacao": {
                    "regex": r"\s*\w*((N|n)otas\s*sobre\s*conservaç(ã|a)o\s*.*\s*(\w*\s*.*)*)\s*",
                },
    "regras_conv": {
                    "regex": r"\s*\w*((R|r)egras\s*e\s*conven(c|ç)(õ|o)es\s*.*\s*(\w*\s*.)*)\s*",
                },
    "data_cri": {
                    "regex": r"\s*\w*((D|d)ata\s*de\s*cria(c|ç)(ã|a)o.\s*revis(a|ã)o\s*e\s*elimina(c|ç)(ã|a)o\s*.*\s*(\w*\s*.)*)\s*",
                },
    "lan2": {
                    "regex": r"\s*\w*((I|i)dioma\s*.*\s*(\w*\s*.)*)\s*",
                },
    "palavras_chaves": {
                    "regex": r"\s*\w*((p|P)ontos\sde\s*acesso\s*e\s*indexa(c|ç)(ã|a)o\s*de\s*assuntos\s*.*\s*(\w*\s*.)*)\s*\w*",
                },
            }

null_dict = {"cod_referencia": None,
             "titulo": None,
             "data": None,
             "nivel_des": None,
             "dim_sup": None,
             "nome_prod": None,
             "conteudo": None,
             "sis_ar": None,
             "cond_ace": None,
             "lan": None,
             "conservacao": None,
             "regras_conv": None,
             "data_cri": None,
             "lan2": None,
             "palavras_chaves": None,
             }
