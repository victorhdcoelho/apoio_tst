import re
import pandas as pd
import numpy as np


class PreProcessingDF:
    def __init__(self, df, atom):
        self.__df = df
        self.__atom = atom

    def split_reference_code(self, legacy_id):
        if legacy_id is None:
            return (None, None, None)
        legacy_id = legacy_id.replace("C ", "C")
        legacy_id = legacy_id.replace("–", "-")
        legacy_id = re.sub(r"\s+", '-', legacy_id, flags=re.IGNORECASE)
        legacy_id = re.sub(r"(-)+$", '', legacy_id, flags=re.IGNORECASE)
        legacy_id = re.sub(r"^(-)+", '', legacy_id, flags=re.IGNORECASE)
        parent_id = "-".join(legacy_id.split("-")[:-1])
        identifier = legacy_id.split("-")[-1]
        return (legacy_id, parent_id, identifier)

    def clean_reference_code(self):
        temp = self.__df["cod_referencia"].apply(
            lambda x: self.split_reference_code(x))

        self.__df["legacyId"] = [each[0] for each in temp]
        self.__df["parentId"] = [each[1] for each in temp]
        self.__df["identifier"] = [each[2] for each in temp]

    def rename_fields(self):
        self.__df = self.__df.rename(
            columns={"titulo": "title",
                     "nivel_des": "levelOfDescription",
                     "dim_sup": "extentAndMedium",
                     "conteudo": "scopeAndContent",
                     "sis_ar": "arrangement",
                     "nome_prod": "EventActors",
                     "cond_ace": "accessConditions",
                     "lan": "language",
                     "regras_conv": "rules",
                     "data_cri": "RevisionHistory",
                     "lan2": "languageOfDescription",
                     "conservacao": "physicalCharacteristics",
                     "id_caixa": "physicalObjectLocation",
                     "data": "eventDates"})

    def join_subject(self, subject):
        if subject is None or subject == np.nan:
            return None
        try:
            subject = "|".join(subject.split(". "))
            return subject
        except Exception:
            return None

    def clean_subject(self):
        self.__df["subjectAccessPoints"] = self.__df["palavras_chaves"].apply(
            lambda x: self.join_subject(x)
        )

    def clean_repository(self):
        self.__df["repository"] = [
            "Tribunal Superior do Trabalho" for each in range(
                self.__df.shape[0])]

    def format_date(self, date):
        if date is None:
            return (None, None)
        if type(date) == str:
            date = str(date)
        else:
            date = date.isoformat()
            date = date.split("T")[0]

        date = re.sub(r"–", "-", date, re.IGNORECASE)
        date = re.sub(r"/", "-", date, re.IGNORECASE)
        date = re.sub(r"\s+", "", date, re.IGNORECASE)
        date = re.sub(r"(,|\.)+", "", date, re.IGNORECASE)
        date = date.split("-")

        if len(date) == 2:
            return ("{}-01-01".format(date[0]), "{}-12-31".format(date[1]))

        elif len(date) > 2:
            if len(date[0]) != 4:
                return ("-".join([date[2], date[1], date[0]]),
                        "-".join([date[2], date[1], date[0]]))
            else:
                return ("-".join([date[0], date[1], date[2]]),
                        "-".join([date[0], date[1], date[2]]))

        else:
            return ("{}-01-01".format(date), "{}-12-31".format(date))

    def clean_date(self):
        temp = self.__df["eventDates"].apply(lambda x: self.format_date(x))
        self.__df["eventStartDates"] = [each[0] for each in temp]
        self.__df["eventEndDates"] = [each[1] for each in temp]

    def drop_unused_columns(self):
        self.__df = self.__df.drop(
            ["palavras_chaves", "cod_referencia"], axis=1)

    def add_template_columns(self, template):
        new_cols = list(set(template.columns) - set(self.__df.columns))
        for each in new_cols:
            self.__df[each] = [None for i in range(self.__df.shape[0])]

    def run_preprocessing(self):
        template = pd.read_csv(self.__atom)
        print(self.__df.head())
        self.clean_reference_code()
        self.__df["culture"] = self.__df["lan"]
        self.rename_fields()
        self.clean_subject()
        self.clean_repository()
        self.clean_date()
        self.drop_unused_columns()
        self.add_template_columns(template)

        return self.__df
