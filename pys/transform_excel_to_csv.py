import sys
import re
import pandas as pd
from tqdm import tqdm
import numpy as np
from static_variables import data, aux, null_dict
from dataframe_pre_processing import PreProcessingDF


class TransformExcel2CSV:
    def __init__(self, path, atom, destiny):
        self.__path = path
        self.__atom = atom
        self.__destiny = destiny

    def __get_length_excel(self):
        excel = pd.ExcelFile(self.__path)
        return len(excel.sheet_names)

    def __extract_docs_from_excel(self, docs_len):
        docs = []
        for index in tqdm(range(1, docs_len)):
            docs.append(pd.read_excel(self.__path, sheet_name=index))

        return docs

    def __get_info_from_regex(self, docs, df):
        """ Zerando a variável para análise de um outro documento !"""
        temp = null_dict
        for index in range(df.shape[0]):
            df.columns = docs[0].columns
            """ Caso a linha esteja vazia"""
            if df["ITEM DE ANÁLISE"].iloc[index] == np.nan:
                continue
            """
                Caso contrário irá tentar encontrar a informação via regex
                (Regular Expression) !
            """
            for key in aux.keys():
                match = re.match(aux[key]["regex"],
                                 str(df["ITEM DE ANÁLISE"].iloc[index]))
                if match:
                    try:
                        temp[key] = df["EXPLICAÇÃO"].iloc[index]
                    except Exception:
                        temp[key] = None

        return temp

    def __transform_docs_to_dataframe(self, docs):
        for df in tqdm(docs):
            temp = self.__get_info_from_regex(docs, df)

            for key in data.keys():
                data[key].append(temp[key])

        return pd.DataFrame(data=data)

    def __read_and_transform_docs_of_excel(self):
        docs_len = self.__get_length_excel()
        docs = self.__extract_docs_from_excel(docs_len)
        print(docs[0])
        df = self.__transform_docs_to_dataframe(docs)
        print(df.head())
        return df

    def save_and_transform_dataframe(self):
        df = self.__read_and_transform_docs_of_excel()
        pre_processing = PreProcessingDF(df, self.__atom)
        clean_df = pre_processing.run_preprocessing()
        clean_df.to_csv(self.__destiny)


if __name__ == "__main__":
    path = sys.argv[1]
    atom = sys.argv[2]
    destiny = sys.argv[3]

    excel_2_csv = TransformExcel2CSV(path, atom, destiny)
    excel_2_csv.save_and_transform_dataframe()
