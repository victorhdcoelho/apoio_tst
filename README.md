# Apoio TST

## Objetivo   
O Objetivo desse projeto é ajudar o TST na conversão de descrições do formato
excel para o formato CSV usando reconhecimento de padrões e formatando de acordo
com o template do atom incluso no projeto.

## Pré requisitos   
Para rodar o projeto tem que haver algumas etapas para configuração do ambiente.

### Etapas
1. Download do python na versão 3.7 ou superior: [python](https://www.python.org/downloads/);
2. Após o python ser instalado verifique se há a ferramenta de gerenciador de pacotes chamada pip
usando o comanda: `pip3 --version`;
3. Utilize o comando: `pip3 install -r requirements.txt` no terminal para ter as bibliotecas necessárias para usar os scripts.
4. Após todos os passos anteriores estárem ok, seu ambiente está pronto para executar o projeto.

## Executando projeto   
Para executar o projeto use o comando `python3 pys/transform_excel_to_csv.py <caminho do excel> <caminho do modelo do atom> <caminho destino.csv>`.   
O modelo do atom que foi seguido está em csvs !